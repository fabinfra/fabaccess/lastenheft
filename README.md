# FabAccess Lastenheft

Ein Lastenheft für das FabAccess Projekt innerhalb der FabInfra Gruppe.

Zur Begriffsklärung - die in diesem Repository gepflegte Datei ist kein reines Lastenheft im Sinne der DIN 69901, auch wenn wir es der Einfachheit halber trotzdem Lastenheft nennen. Es ist vielmehr wie in vielen anderen Projekten ein möglichst effizienter aber doch umfassender Kompromiss aus Lasten- und Pflichtenheft. Dabei liegt der Schwerpunkt des Lastenhefts in diesem Dokument, der Schwerpunkt des Pflichtenhefts findet sich eher im umgesetzten Code.

Dass wir ein Lastenheft schreiben bedeutet auch nicht, dass wir keine agile Entwicklung betreiben. Auch im Rahmen einer agilen Entwicklung sind Lastenhefte bzw. Anforderungslisten hilfreiche Dokumente, die allerdings im Gegensatz zum "klassischen Wasserfallmodell" nicht statisch sind, sondern mit fortschreitendem Projektverlauf sowohl angepasst als auch stärker detailliert werden.

Dieses Lastenheft hilft bei der Strukturierung und Fokussierung der Arbeit und der Aufgabenverteilung im Projekt-Team.

Das Lastenheft beinhaltet zunächst alle Dinge, die vom Projektteam in verschiedenen Stufen (major version) umgesetzt werden sollen. Erkenntnisse aus der Phase der Materialsammlung sind - so weit möglich und sinnvoll - bei der Erarbeitung des Lastenhefts eingeflossen.

Ein Pflichtenheft wird zunächst nicht erstellt - der erstellte Code spiegelt dynamisch das Pflichtenheft wieder.

Für Interessenten aus offenen Werkstätten oder FabLabs, die nicht programmieren können / wollen, aber gerne ihre Ideen einbringen, gibt es verschiedene Varianten um zum Projekt beizutragen:
1. Du hast einen Wunsch, was die Software unbedingt können muss, also einen "feature request".
    In dem Fall kannst Du gerne [hier](https://gitlab.com/fabinfra/fabaccess_lastenheft/-/issues) einen Issue im Repository zum Lastenheft erstellen. Genauere Hinweise findest Du in der Datei "[CONTRIBUTING.md](https://gitlab.com/fabinfra/fabaccess_lastenheft/-/blob/master/CONTRIBUTING.md)".
1. Du hast einen Fehler im Programm oder seiner Funktion gefunden, also einen "bug report".
    In dem Fall führt Dich Dein Weg direkt in das Repository des entsprechenden Programmteils (bei FabAccess z.B. [dort](https://gitlab.com/fabinfra/fabaccess/-/issues/)).
   
Wenn Du Hilfe brauchst, kannst Du Dich gerne jederzeit an einen der Owner des Repositorys wenden.

# PDF Lastenheft
[Hier](https://fabinfra.gitlab.io/fabaccess/lastenheft/FabAccess_Lastenheft.pdf) kannst du das Lastenheft als PDF herrunterladen.

Hinweis: Das Lastenheft wird bei jeder Änderung automatisch per CI/CD Job durch [.gitlab-ci.yml](https://gitlab.com/fabinfra/fabaccess/lastenheft/-/blob/master/.gitlab-ci.yml?ref_type=heads) gebaut und via [GitLab Pages](https://gitlab.com/fabinfra/fabaccess/lastenheft/pages) bereitgestellt.
