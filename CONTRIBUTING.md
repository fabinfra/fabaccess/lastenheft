# Contribute

Danke das du bei dem Lastenheft mithelfen möchtest.

Falls du ein Feature -- Anforderung in der Spezifikation oder erweiterte Funktionalität -- benötigst das noch nicht berücksichtigt wurde, schau erst einmal nach, ob zu deinem Wunsch bereits ein Issue für so ein Feature geöffnet wurde. Falls dem so ist gib dem Issue einen Daumen hoch o.ae. um uns zu zeigen das an dem Feature vermehrt Interesse besteht. 
Falls du leicht andere oder erweiterte Anforderungen hast schreibe einen Kommentar in den Issue in dem du diese darlegst, damit wird sie berücksichtigen können.

Bitte vermeide Kommentare wie "Ich brauche das Feature auch", da solche nur unnötiges Rauschen erzeugen.

Bitte behalte bei alle dem im Hinterkopf das wir begrenzt viel Zeit und Ressourcen besitzen. Einige Anforderungen können von uns nicht berücksichtigt werden bzw. sollten unserer Meinung nach von externen Tools gelöst werden.

Bitte nimm also Kommentare in die Richtung oder Schließungen deines Issue nicht persöhnlich -- sowas ist einfach unseren mangelnden Kapazitäten geschuldet.

Gleiches gilt auch falls wir auf ein Issue einmal deiner Meinung nach zu langsam reagieren. Es ist nach wie vor ein Hobbyprojekt der meisten beteiligten Entwickler.
